'use strict';

import gulp from 'gulp';
import compass from 'gulp-compass';
import modernizr from 'gulp-modernizr';
import babel from 'gulp-babel';
import pug from 'gulp-pug';
import wrapamd from 'gulp-wrap-amd';
import browserify from 'browserify';
import source from 'vinyl-source-stream';
import streamify from 'gulp-streamify';
import uglify from 'gulp-uglify';
import babelify from 'babelify';
import deamdify from 'deamdify';
import imagemin from 'gulp-imagemin';

/**
 * File paths of the application
 * @type {{src: {sass: string, js: string, serverjs: string, views: string, templates: string, images: string, frontapp: string}, dest: {css: string, sass: string, js: string, serverjs: string, views: string, templates: string, images: string}}}
 */
const paths = {
    src: {
        sass: 'src/front/sass/*.scss',
        js: 'src/front/js/**/*.js',
        serverjs: 'src/server/**/*.js',
        views: 'src/server/views/**/*.pug',
        templates: 'src/front/js/templates/**/*.pug',
        images: 'src/front/img/**/*',
        frontapp: './src/front/js/app.js'
    },
    dest: {
        css: 'dist/webroot/css',
        sass: 'src/front/sass',
        js: 'dist/webroot/js',
        serverjs: 'dist/app',
        views: 'dist/app/views',
        templates: 'src/front/js/templates/compiled',
        images: 'dist/webroot/img'

    }
};

/**
 * Compass compile scss files
 */
gulp.task('compass', () => {
    gulp.src(paths.src.sass)
        .pipe(compass({
            config_file: './config.rb',
            css: paths.dest.css,
            sass: paths.dest.sass
        }))
        .pipe(gulp.dest(paths.dest.css));
});

/**
 * Build cutom modernizr with only the used prefixes
 */
gulp.task('modernizr', () => {
    gulp.src([paths.src.js, paths.src.sass])
        .pipe(modernizr())
        .pipe(uglify())
        .pipe(gulp.dest(paths.dest.js));
});

/**
 * Babel transpile the server app
 */
gulp.task('babel', () => {
    gulp.src(paths.src.serverjs)
        .pipe(babel())
        .pipe(gulp.dest(paths.dest.serverjs));
});

/**
 * Copy pug templates
 */
gulp.task('views', () => {
    gulp.src(paths.src.views)
        .pipe(gulp.dest(paths.dest.views));
});

/**
 * Precompile pug template to be used on the front into AMD modules
 */
gulp.task('templates', () => {
    gulp.src(paths.src.templates)
        .pipe(pug({
            client: true,
            debug: false,
            compileDebug: false
        }))
        .pipe(wrapamd({
            exports: 'template'
        }))
        .pipe(gulp.dest(paths.dest.templates));
});

/**
 * Bundle front app: Babel, DeAMDify, Browserify, Uglify
 */
gulp.task('bundle', () => {
    browserify({entries: paths.src.frontapp, extensions: ['.js'], debug: false})
        .transform(babelify)
        .transform(deamdify)
        .bundle()
        .pipe(source('app.js'))
        .pipe(streamify(uglify()))
        .pipe(gulp.dest(paths.dest.js));
});

/**
 * Optimize images
 */
gulp.task('images', () => {
    gulp.src(paths.src.images)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dest.images));
});

/**
 * Watch for changes
 */
gulp.task('watch', () => {
    gulp.watch(paths.src.sass, ['compass']);
    gulp.watch(paths.src.js, ['bundle']);
    gulp.watch(paths.src.images, ['images']);
    gulp.watch(paths.src.templates, ['templates']);
});

/**
 * build the frontend app
 */
gulp.task('front', ['compass', 'modernizr', 'templates', 'images', 'bundle']);

/**
 * Build the server app
 */
gulp.task('server', ['babel', 'views']);

/**
 * Build the whole thing
 */
gulp.task('default', ['front', 'server']);
