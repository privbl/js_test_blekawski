import koa from 'koa';
import koapug from 'koa-pug';
import koastatic from 'koa-static';
import router from './routes';
import uacompatible from './middleware/uacompatible';
import _ from 'lodash';

/**
 * Initialize the application
 */

/**
 * Main Koa application
 */
var app = koa();

/**
 * Pug template engine middleware
 * @type {Pug}
 */
var pug = new koapug({
    viewPath: __dirname + '/views',
    debug: false,
    pretty: false,
    compileDebug: false,
    helperPath: [
        {_: _}
    ]
});
pug.use(app);
app.use(koastatic(__dirname + '/../webroot', {
    maxage: 365 * 24 * 60 * 60
}));
app.use(uacompatible());
app.use(router.routes());

app.listen(3000);