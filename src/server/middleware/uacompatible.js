/**
 * Sample middleware. Add X-UA-Compatible header to all responses
 * @returns {uacompatible}
 */
export default function uacompatible(){
    /**
     * Add X-UA-Compatible header to all responses
     */
    return function *uacompatible(next) {
        yield next;
        this.set('X-UA-Compatible', 'IE=edge');
    };
};

