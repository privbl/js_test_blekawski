/**
 * Main page controller
 */
export default {
    home: {
        /**
         * Handle get requests to /.
         * Render home template
         */
        * get() {
            this.render('home.pug', {title: "Address Book"}, true);
        }
    }
}