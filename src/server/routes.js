import koarouter from 'koa-router';
import controllers from './controllers';

/**
 * Main app router
 * @type {Router}
 */
var router = new koarouter({
    prefix: '/'
});

//route definitions
router.get('/', controllers.home.get);

export default router;