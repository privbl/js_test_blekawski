/**
 * Is the logger on?
 * @type {boolean}
 * @private
 */
let _on = false;

/**
 * Helper wrapper for console.
 */
export default class Logger{
    /**
     * check if logging is currently enabled
     * @returns {boolean} - enabled or not
     */
    static get enabled(){
        return _on;
    }

    /**
     * Turn logging on.
     */
    static enable(){
        _on = true;
    }

    /**
     * Turn logging off
     */
    static disable(){
        _on = false;
    }

    /**
     * Log errors.
     * @param params - params to log to console
     */
    static error(...params){
        if(_on){
            console.warn(params);
        }
    }

    /**
     * Log messages.
     * @param params - params to log to console
     */
    static log(...params){
        if(_on){
            console.log(params);
        }
    }
}