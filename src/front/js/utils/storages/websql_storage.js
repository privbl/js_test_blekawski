import Storage from './storage';
import Logger from '../logger';

/**
 * Storage engine based on WebSQLDatabase
 */
export default class WebsqlStorage extends Storage{
    /**
     * @param {String} modelname - class name of the model for which the storage should be created
     */
    constructor(modelname){
        super(modelname);
        try {
            this.storage = openDatabase('storage', '1.0', '', 2 * 1024 * 1024);
            this.engine = 'websql';
        } catch (e) {
            Logger.error('Error creating websql database', e);
        }
    }

    /**
     * Persistently store the data in the browser
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    persist() {
        return new Promise((mainResolve, mainReject) => {
            //inspect object fields. It is assumed storage holds objects of same class
            let firstItem = this.data.values().next().value;
            let fields = [];

            for (let prop in firstItem) {
                if (prop !== 'id' && prop !== 'modelName' && prop !== 'dirty' && prop !== '') {
                    fields.push(prop);
                }
            }
            let fieldsString = fields.join(',');
            this.storage.transaction((tx) => {
                (new Promise((createResolve, createReject) => {
                    //create web sql table
                    Logger.log('WbSQL', `CREATE TABLE IF NOT EXISTS ${this.name} (id unique, ${fieldsString})`);
                    tx.executeSql(`CREATE TABLE IF NOT EXISTS ${this.name} (id unique, ${fieldsString})`, null, () => {
                        createResolve();
                    }, (tr, error) => {
                        createReject(error);
                    });
                })).then(() => {
                    //iterate over items, check for existence and either update or add new to the table
                    let operationPromises = [];
                    for (let model of this.data.values()) {
                        if (model.dirty) {
                            operationPromises.push(new Promise((resolve, reject) => {
                                Logger.log('WbSQL', `SELECT id FROM ${this.name} WHERE id = ?`, [model.id]);
                                tx.executeSql(`SELECT id FROM ${this.name} WHERE id = ?`, [model.id], (tx, results) => {
                                    if (results.rows.length > 0) {
                                        //if model is in db, do update
                                        let updateValues = [];
                                        for (let prop of fields) {
                                            updateValues.push(prop + '=\'' + model[prop] + '\'');
                                        }
                                        let updateString = updateValues.join(',');
                                        Logger.log('WbSQL', `UPDATE ${this.name} SET ${updateString} WHERE id = ?`, [model.id]);
                                        tx.executeSql(`UPDATE ${this.name} SET ${updateString} WHERE id = ?`, [model.id], (tr, results) => {
                                            resolve();
                                        }, (tr, error) => {
                                            reject(error);
                                        });
                                    } else {
                                        //insert new item
                                        let values = [];
                                        values.push('\'' + model.id + '\'');
                                        for (let prop of fields) {
                                            values.push('\'' + model[prop] + '\'');
                                        }
                                        let valuesString = values.join(',');
                                        Logger.log('WbSQL', `INSERT INTO ${this.name} (id, ${fieldsString}) VALUES (${valuesString})`);
                                        tx.executeSql(`INSERT INTO ${this.name} (id, ${fieldsString}) VALUES (${valuesString})`, null, (tr, results) => {
                                            resolve();
                                        }, (tr, error) => {
                                            Logger.error('WbSQL', error);
                                            reject();
                                        });
                                    }

                                }, (tr, error) => {
                                    reject(error);
                                });
                            }));
                            model.dirty = false;
                        }
                    }
                    //check if there is anything to remove, and remove if needed
                    for (let [idx, idToRemove] of this.idsToRemove.entries()) {
                        operationPromises.push(new Promise((delResolve, delReject) => {
                            Logger.log('WbSQL', `DELETE FROM ${this.name} WHERE id = ?`, [idToRemove]);
                            tx.executeSql(`DELETE FROM ${this.name} WHERE id = ?`, [idToRemove], (tr, results) => {
                                delResolve();
                            }, (tr, error) => {
                                delReject(error);
                            });
                        }));
                    }
                    if(operationPromises.length > 0) {
                        return Promise.all(operationPromises);
                    } else {
                        return Promise.resolve();
                    }
                }).then(() => {
                    mainResolve();
                }).catch((error)=> {
                    Logger.error('WbSQL', error);
                    mainReject();
                })
            });
        });
    }

    /**
     * Load the data from the browser into the storage engine
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    read() {
        this.data = new Map();
        return new Promise((resolve, reject) => {
            this.storage.transaction(tx => {
                Logger.log('WbSQL', `SELECT * FROM ${this.name}`);
                tx.executeSql(`SELECT * FROM ${this.name}`, null, (tx, results) => {
                    if (results.rows.length > 0) {
                        for (let row of results.rows) {
                            var modelObject = this.createModel(row);
                            this.data.set(modelObject.id, modelObject);
                        }
                        resolve(this.data)
                    }
                }, (tr, error) => {
                    Logger.error('WbSQL', error);
                    reject(error);
                });
            });
        });
    }
}