import LocalStorage from './local_storage';
/**
 * Storage engine based on sessionStorage
 */
export default class SessionStorage extends LocalStorage{
    /**
     * @param {String} modelname - class name of the model for which the storage should be created
     */
    constructor(modelname){
        super(modelname);
        this.storage = window.sessionStorage;
        this.engine = 'session';
    }
}