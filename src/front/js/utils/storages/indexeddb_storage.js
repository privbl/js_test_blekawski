import Storage from './storage';
import Logger from '../logger';

/**
 * Storage engine based on IndexedDB
 */
export default class IndexeddbStorage extends Storage{
    /**
     * @param {String} modelname - class name of the model for which the storage should be created
     */
    constructor(modelname){
        super(modelname);
        this.storage = false;
        this.engine = 'indexeddb';
    }
    
    /**
     * Initialize the IndexedDB
     * @returns {Promise}
     */
    init() {
        return new Promise((resolve, reject) => {
            if (!this.storage) {
                Logger.log('IndexedDB open db');
                let open = window.indexedDB.open('storage', 3);
                open.onupgradeneeded = (event) => {
                    let db = event.target.result;
                    let objectStore = db.createObjectStore(this.name, {keyPath: "id"});
                    objectStore.transaction.onerror = (err) => {
                        Logger.error('IndexedDB', err);
                    };
                };
                open.onsuccess = (event) => {
                    this.storage = event.target.result;
                    resolve();
                }
            } else {
                resolve();
            }
        });
    }
    
    /**
     * Persistently store the data in the browser
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    persist() {
        return this.init().then(() => {
            let operationPromises = [];

            for (let model of this.data.values()) {
                operationPromises.push(new Promise((resolve, reject) => {
                    if (model.dirty) {
                        let objectStore = this.storage.transaction([this.name], "readwrite").objectStore(this.name);
                        let req = objectStore.get(model.id);
                        req.onerror = (err) => {
                            Logger.error('IndexedDB', err);
                            reject();
                        };
                        req.onsuccess = function (event) {
                            var data = req.result;
                            if (typeof data === 'undefined') {
                                //object doesn't exist, we need to create it
                                Logger.log('IndexedDB add item');
                                let insert = objectStore.add(model);
                                insert.onerror = (err) => {
                                    Logger.error('IndexedDB', err);
                                    reject(err);
                                };
                                insert.onsuccess = (event) => {
                                    resolve();
                                };
                            } else {
                                //object exists, we need to update it
                                Logger.log('IndexedDB update item');
                                let update = objectStore.put(model);
                                update.onerror = (err) => {
                                    Logger.error('IndexedDB', err);
                                    reject(err);
                                };
                                update.onsuccess = (event) => {
                                    resolve();
                                };
                            }
                        };
                        model.dirty = false;
                    } else {
                        resolve();
                    }
                }));
            }

            //check if there is anything to remove, and remove if needed
            for (let [idx, idToRemove] of this.idsToRemove.entries()) {
                operationPromises.push(new Promise((delResolve) => {
                    Logger.log('IndexedDB delete item', [idToRemove]);
                    let objectStore = this.storage.transaction([this.name], "readwrite").objectStore(this.name);
                    objectStore.delete(idToRemove);
                    delResolve();
                }));
            }

            return Promise.all(operationPromises);
        });
    }

    /**
     * Load the data from the browser into the storage engine
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    read() {
        return this.init().then(() => {
            return new Promise((resolve, reject) => {
                this.data = new Map();
                let objectStore = this.storage.transaction([this.name], "readwrite").objectStore(this.name);
                let cursor = objectStore.openCursor();
                cursor.onerror = (err) => {
                    Logger.error('IndexedDB', err);
                    reject();
                };
                cursor.onsuccess = (event) => {
                    var cursor = event.target.result;
                    if (cursor) {
                        this.data.set(cursor.value.id, this.createModel(cursor.value));
                        cursor.continue();
                    }
                    else {
                        resolve(this.data);
                    }
                };
            });
        });
    }
}