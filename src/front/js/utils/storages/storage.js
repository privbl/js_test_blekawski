import models from '../../models';

/**
 * Helper to store model ids marked for removal
 * @type {Array}
 * @private
 */
let _idsToRemove = [];

/**
 * In browser storage engine.
 */
export default class Storage {
    /**
     * @param {String} modelname - class name of the model for which the storage should be created
     */
    constructor(modelname) {
        /**
         * Has anything been changed since last persist call?
         * @type {boolean}
         */
        this.dirty = true;
        /**
         * Model name
         * @type {String}
         */
        this.name = modelname;
        /**
         * Actual underlying storage engine
         * @type {boolean}
         */
        this.storage = false;
        /**
         * Underlying storage name
         * @type {string}
         */
        this.engine = 'none';
    }

    /**
     * @returns {Array} - Model ids marked for removal
     */
    get idsToRemove() {
        return _idsToRemove;
    }

    /**
     * Get model by id
     * @param {String} id - id of the model to get
     * @returns {Promise.<Model>} - Promise resolving to a model
     */
    get(id) {
        if (this.dirty) {
            this.read().then(() => {
                this.dirty = false;
                return Promise.resolve(this.data.get(id));
            });
        } else {
            return Promise.resolve(this.data.get(id));
        }
    }

    /**
     * Get all stored models
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    all() {
        if (this.dirty) {
            let dataPromise = this.read();
            this.dirty = false;
            return dataPromise;
        }
        return Promise.resolve(this.data);
    }

    /**
     * Store a model
     * @param {String} id - id of the model to store
     * @param {Model} model - the actual model to store
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    add(id, model) {
        return this.update(id, model);
    }

    /**
     * Update an existing, already stored model
     * @param {String} id - id of the model to update
     * @param {Model} model - the actual model to update
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    update(id, model) {
        this.data.set(id, model);
        return this.persist();
    }

    /**
     * Remove a model with the given id
     * @param {String} id - the model id to remove
     * @returns {Promise} - Promise
     */
    remove(id) {
        this.data.delete(id);
        this.markForRemoval(id);
        return this.persist().then(() => {
            //all correct, all items removed
            _idsToRemove = [];
        });
    }

    /**
     * Mark the id for removal
     * @param {String} id - the model id to remove
     */
    markForRemoval(id) {
        _idsToRemove.push(id);
    }

    /**
     * Persistently store the data in the browser
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    persist() {
        //no storage available, initialize volatile storage
        if (!this.data) {
            this.data = new Map();
        }
        this.dirty = true;
        return Promise.resolve(this.data);
    }

    /**
     * Load the data from the browser into the storage engine
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    read() {
        //no storage available, initialize volatile storage
        if (!this.data) {
            this.data = new Map();
        }
        return Promise.resolve(this.data);
    }

    /**
     * Create a map of proper models form a serialized string
     * @param {String} serializedData - a serialized Map of models
     */
    readFromSerializedData(serializedData){
        let tempData = new Map(JSON.parse(serializedData));

        this.data = new Map();
        for(let [id, obj] of tempData){
            this.data.set(id, this.createModel(obj));
        }
    }

    /**
     * Create a proper Model from a simple object
     * @param {Object} obj - an object to convert to a Model
     */
    createModel(obj){
        //todo: es6-module-loader doesn't currently work with babel 6. Use System.import when possible.
        let ModelClass = models[this.name];
        return new ModelClass(obj);
    }
}