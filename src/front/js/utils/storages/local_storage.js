import Storage from './storage';
import models from '../../models';

/**
 * Storage engine based on localStorage
 */
export default class LocalStorage extends Storage{
    /**
     * @param {String} modelname - class name of the model for which the storage should be created
     */
    constructor(modelname){
        super(modelname);
        this.storage = window.localStorage;
        this.engine = 'local';
    }
    
    /**
     * Persistently store the data in the browser
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    persist() {
        return new Promise((resolve, reject) => {
            try {
                let serializedData = JSON.stringify(this.data);
                this.storage.setItem(this.name, serializedData);
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    }

    /**
     * Load the data from the browser into the storage engine
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    read() {
        return new Promise((resolve, reject) => {
            try {
                let serializedData = this.storage.getItem(this.name);
                if (serializedData) {
                    this.readFromSerializedData(serializedData);
                } else {
                    this.data = new Map();
                }
                resolve(this.data);
            } catch (e) {
                reject(e);
            }
        });
    }
}