import Storage from './storage';
import cookie from 'cookie';

/**
 * The size limit in bytes of the underlying cookie
 * @type {number}
 */
const COOKIE_SIZE_LIMIT = 4000;

export default class CookieStorage extends Storage{
    /**
     * @param {String} modelname - class name of the model for which the storage should be created
     */
    constructor(modelname){
        super(modelname);
        this.storage = document.cookie;
        this.engine = 'cookie';
    }

    /**
     * Calculate byte size of an utf8 string
     * src: http://stackoverflow.com/a/23329386
     * @param {String} str - string to calculate
     * @returns {number} the byte size of the string
     */
    stringLength(str){
        var s = str.length;
        for (var i = str.length - 1; i >= 0; i--) {
            var code = str.charCodeAt(i);
            if (code > 0x7f && code <= 0x7ff) s++;
            else if (code > 0x7ff && code <= 0xffff) s += 2;
            if (code >= 0xDC00 && code <= 0xDFFF) i--;
        }
        return s;
    }
    
    /**
     * Persistently store the data in the browser
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    persist() {
        return new Promise((resolve, reject) => {
            try {
                let serializedData = cookie.serialize(this.name, JSON.stringify(this.data));
                let len = this.stringLength(serializedData);

                if (len <= COOKIE_SIZE_LIMIT) {
                    document.cookie = serializedData;
                    resolve();
                } else {
                    reject("Cookie size exceeded");
                }
            } catch (e) {
                reject(e);
            }
        });
    }

    /**
     * Load the data from the browser into the storage engine
     * @returns {Promise.<Map>} - Promise resolving to a Map of models
     */
    read() {
        return new Promise((resolve, reject) => {
            try {
                let serializedData = cookie.parse(document.cookie)[this.name];
                if (serializedData) {
                    this.readFromSerializedData(serializedData);
                } else {
                    this.data = new Map();
                }
                resolve(this.data);
            } catch (e) {
                reject(e);
            }
        });
    }
}