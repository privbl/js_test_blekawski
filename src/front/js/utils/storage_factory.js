import WebsqlStorage from './storages/websql_storage';
import IndexeddbStorage from './storages/indexeddb_storage';
import LocalStorage from './storages/local_storage';
import SessionStorage from './storages/session_storage';
import CookieStorage from './storages/cookie_storage';
import Storage from './storages/storage';

/**
 * Decides which storage engine to use based on browser availability.
 * Checked with Modernizr.
 */
export default class StorageFactory {
    /**
     * Creates a storage engine.
     * @param {String} modelname - class name of the model for which the storage should be created
     * @returns {Storage} - storage object
     */
    static create(modelname) {
        if (Modernizr.websqldatabase) {
            return new WebsqlStorage(modelname);
        } else if (Modernizr.indexeddb) {
            return new IndexeddbStorage(modelname);
        } else if (Modernizr.localstorage) {
            return new LocalStorage(modelname);
        } else if (Modernizr.sessionstorage) {
            return new SessionStorage(modelname);
        } else if (Modernizr.cookies) {
            return new CookieStorage(modelname);
        }
        return new Storage(modelname);
    }
}