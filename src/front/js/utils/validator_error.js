/**
 * Models validation error
 */
export default class ValidatorError extends Error {
    /**
     * @param {String} message - error message
     * @param {Array.<String>} fields - array of fields marked as invalid
     */
    constructor(message, fields) {
        super(message);
        this.fields = fields;
        this.name = 'ValidatorError';
        if (typeof Error.captureStackTrace === 'function') {
            Error.captureStackTrace(this, this.constructor);
        } else {
            this.stack = (new Error(message)).stack;
        }
    }
}