import Controller from './controller';
import Logger from '../utils/logger';
import Contact from '../models/contact';
import ContactCollection from '../collections/contacts';
import ValidatorError from '../utils/validator_error';

import countryList from 'country-list';

import templateRow from '../templates/compiled/contacts/row';
import templateEdit from '../templates/compiled/contacts/edit';
import templateDelete from '../templates/compiled/contacts/delete';

/**
 * Controller dealing with contacts manipulation
 */
export default class ContactsController extends Controller {
    /**
     * @param displayElement - the css path to main element bound to this controller
     */
    constructor(displayElement) {
        super();
        /**
         * the css path to main element bound to this controller
         */
        this.displayElement = displayElement;
        /**
         * @type {ContactCollection} collection of Contact models
         */
        this.collection = new ContactCollection();
        this.list();

        //register all event handlers for the current view
        let click = new Map();
        click.set('#add', this.edit);
        click.set('body.edit', event => {
            if (event.target.classList.contains('edit')) {
                let id = event.target.dataset.id;
                this.edit(event, id);
            }
        });
        click.set('body.del', event => {
            if (event.target.classList.contains('delete')) {
                let id = event.target.dataset.id;
                this.deleteAsk(event, id);
            }
        });
        click.set('body.fdel', event => {
            if (event.target.id === 'final-delete') {
                let id = event.target.dataset.id;
                this.delete(event, id);
            }
        });
        click.set('body.close', event => {
            if (event.target.classList.contains('close')) {
                this.close(event);
            }
        });
        let submit = new Map();
        submit.set('body.save', event => {
            if (event.target.id === 'edit-contact') {
                this.save(event);
            }
        });

        this.events.set('click', click);
        this.events.set('submit', submit);
        this.bindEvents();
    }

    /**
     * Display all contacts
     */
    list() {
        this.collection.all().then((allContact) => {
            let i = 0;
            let html = "";
            for (let [ id, contact ] of allContact.entries()) {
                i++;
                //render contact row
                html += templateRow({
                    counter: "" + i,
                    id: id,
                    firstName: contact.firstName,
                    lastName: contact.lastName,
                    email: contact.email,
                    country: contact.country
                });
            }

            document.querySelector(this.displayElement).innerHTML = html;
        });
    }

    /**
     * Show the contact edit form and prefill with data if applicable
     * @param {Event} event - the event that triggered this function
     * @param {String|boolean} id - the id of the Contact to modify of false if adding new
     */
    edit(event, id = false) {
        event.preventDefault();
        if(document.getElementById('edit-contact')) {
            document.getElementById('actions').removeChild(document.getElementById('edit-contact'));
        }
        //populate country select
        let ctrList = countryList();
        let data = {
            countries: ctrList.getNames()
        };
        if (id) {
            //this is an edit attempt. Populate form with esiting contacts data.
            this.collection.load(id).then((contact) => {
                data['id'] = id;
                data['firstName'] = contact.firstName;
                data['lastName'] = contact.lastName;
                data['email'] = contact.email;
                data['country'] = contact.country;
                //render edit form
                document.getElementById('actions').innerHTML = templateEdit(data);
            });
        } else {
            //render edit form
            document.getElementById('actions').innerHTML = templateEdit(data);
        }
    }

    /**
     * Save the data from the edit form into a storage engine
     * @param {Event} event - the event that triggered this function
     */
    save(event) {
        event.preventDefault();

        //read form values
        let id = document.getElementById('contactid').value;
        let firstName = document.getElementById('firstName').value;
        let lastName = document.getElementById('lastName').value;
        let email = document.getElementById('email').value;
        let countrySelect = document.getElementById('country');
        let country = countrySelect.options[countrySelect.selectedIndex].value;

        //clear validation info
        for (let input of document.querySelectorAll('input, select')) {
            input.classList.remove('error');
        }

        new Promise((resolve, reject) => {
            let contact;
            if (id) {
                //this is an edit attempt. Load existing contact and modify it
                this.collection.load(id).then((contact) => {
                    contact.firstName = firstName;
                    contact.lastName = lastName;
                    contact.email = email;
                    contact.country = country;
                    resolve(contact);
                }).catch(() => {
                    reject();
                })
            } else {
                //this is new addition. Create totally new contact
                contact = new Contact(false, firstName, lastName, email, country);
                resolve(contact);
            }
        }).then((contact) => {
            //try to save contact data to storage
            return this.collection.update(contact);
        }).then(() => {
            //redraw items list
            this.list();
            //remove edit form
            document.getElementById('actions').removeChild(document.getElementById('edit-contact'));
        }).catch((errors) => {
            //validation error found
            for (let field of errors) {
                //mark field as wrong
                document.getElementById(field).classList.add('error');
            }
            Logger.error('Error saving contact', errors);
        });
    }

    /**
     * Show the delete confirmation window
     * @param {Event} event - the event that triggered this function
     * @param {String} id - id of the Contact to be deleted
     */
    deleteAsk(event, id) {
        event.preventDefault();

        if (id) {
            let contact = this.collection.load(id).then((contact) => {
                document.getElementById('actions').innerHTML = templateDelete({
                    id: id,
                    firstName: contact.firstName,
                    lastName: contact.lastName
                });
            });
        }
    }

    /**
     * Delete a Contact from a storage engine
     * @param {Event} event - the event that triggered this function
     * @param {String} id - id of the Contact to be deleted
     */
    delete(event, id) {
        event.preventDefault();

        if (id) {
            this.collection.load(id).then((item) => {
                this.collection.delete(item).then(() => {
                    this.list();
                    document.getElementById('actions').innerHTML = "";
                });
            });
        }
    }

    /**
     * Hide the current action - edit or delete form
     * @param {Event} event - the event that triggered this function
     */
    close(event) {
        event.preventDefault();

        document.getElementById('actions').innerHTML = "";
    }
}