import on from 'dom-event';

/**
 * Controller
 */
export default class Controller {
    constructor() {
        /**
         * Events map {event type: [{selector: handler function}]}
         * @type {Map}
         */
        this.events = new Map();
    }

    /**
     * Bind all events mapped to this controller
     */
    bindEvents() {
        for (let [eventName, map] of this.events) {
            for (let [identifier, handler] of map) {
                let selector, namespace;
                [selector, namespace] = identifier.split('.');
                for (let element of document.querySelectorAll(selector)) {
                    on(element, eventName, handler);
                }
            }
        }
    }
}