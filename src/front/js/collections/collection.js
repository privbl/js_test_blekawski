import StorageFactory from '../utils/storage_factory';

/**
 * A storage for this collection
 * @type {Storage|boolean}
 * @private
 */
let _storage = false;

/**
 * Collection of Models
 */
export default class Collection {
    constructor() {
        /**
         * Name of Models this collection holds
         * @type {string}
         */
        this.modelName = 'Model';
    }

    /**
     * A storage for this collection
     * @returns {Storage}
     */
    get storage() {
        if (!_storage) {
            _storage = StorageFactory.create(this.modelName);
        }
        return _storage;
    }

    /**
     * Get all Models
     * @returns {Promise.<Map>} - A promise resolving to a Map of all Models
     */
    all() {
        return this.storage.all();
    }

    /**
     * Read in the Models form underlying storage
     * @returns {Promise.<Map>} - A promise resolving to a Map of all Models
     */
    load() {
        return this.storage.read();
    }

    /**
     * Get a specific Model by id
     * @param {String} id - id of the Model to get
     * @returns {Promise.<Model>} - A promise resolving to a Model
     */
    load(id) {
        return this.storage.get(id);
    }

    /**
     * Update many Models with ame data at once
     * @param {Array.<Model>} objects - array of Models to update
     * @param {Map.<*>} values - Map of values to set
     */
    bulkupdate(objects, values) {
        for (let object of objects) {
            for (let [key, value] of values) {
                object[key] = value;
            }
            this.update(object);
        }
    }

    /**
     * Store an updated Model in storage
     * @param {Model} object - Model to store
     * @returns {Promise}
     */
    update(object) {
        return new Promise((resolve, reject) => {
            let valid = object.valid();
            if (valid === true) {
                object.dirty = true;
                this.storage.update(object.id, object).then(() => {
                    object.dirty = false;
                    resolve();
                });
            } else {
                reject(valid);
            }
        });

    }

    /**
     * Delete Model from storage
     * @param {Model} object - Model to delete
     * @returns {Promise}
     */
    delete(object) {
        return this.storage.remove(object.id);
    }
}