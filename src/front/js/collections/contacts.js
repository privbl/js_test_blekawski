import Collection from './collection';

/**
 * Collection of Contact models
 */
export default class ContactCollection extends Collection {
    constructor() {
        super();
        this.modelName = 'Contact';
    }
}