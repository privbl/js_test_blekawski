import Logger from './utils/logger';
import ContactsController from './controllers/contacts';

/**
 * Enable logging to console
 */
Logger.enable();

/**
 * Init Contacts controller and expose publicly
 * @type {ContactsController}
 */
window.contactsController = new ContactsController('#contact_list');

