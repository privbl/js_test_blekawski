import Model from './model';
import isEmail from 'validator/lib/isEmail';

/**
 * Contact model
 */
export default class Contact extends Model {
    /**
     * @param {String} id
     * @param {String} firstName
     * @param {String} lastName
     * @param {String} email
     * @param {String} country
     */
    constructor(id = false, firstName = '', lastName = '', email = '', country = '') {
        super('Contact', id);

        if(typeof id !== 'object') {
            /**
             * First name
             * @type {String}
             */
            this.firstName = firstName;
            /**
             * Last name
             * @type {String}
             */
            this.lastName = lastName;
            /**
             * Email address
             * @type {String}
             */
            this.email = email;
            /**
             * Country
             * @type {String}
             */
            this.country = country;
        }
    }

    /**
     * Check if the contact is correct
     * @returns {Array|boolean} array of invalid fields or true when valid
     */
    valid() {
        let errors = [];
        if (this.firstName.trim() === '') {
            errors.push('firstName');
        }
        if (this.lastName.trim() === '') {
            errors.push('lastName');
        }
        if (this.country.trim() === '') {
            errors.push('country');
        }
        if (this.email.trim() === '') {
            errors.push('email');
        }
        if (errors.indexOf('email') === -1 && !isEmail(this.email)) {
            errors.push('email');
        }
        if (errors.length > 0) {
            return errors;
        }
        return true;
    }
}