/**
 * A data object model
 */
export default class Model {
    /**
     * @param {String} modelName - name of the model
     * @param {String|boolean} id - id of the model or false, when it should be generated
     */
    constructor(modelName, id = false) {
        /**
         * Model name
         * @type {String}
         */
        this.modelName = modelName;
        /**
         * Has this model been manipulated since last persist call
         * @type {boolean}
         */
        this.dirty = false;
        if(typeof id === 'object'){
            for(let prop of Object.keys(id)){
                this[prop] = id[prop];
            }
        } else if (id) {
            this.id = id;
        } else {
            this.id = modelName + Date.now();
        }
    }

    /**
     * Check if the model is valid
     * @returns {boolean} valid or not
     */
    valid() {
        return true;
    }
}