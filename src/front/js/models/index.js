import Contact from './contact';

/**
 * Model class registry that replaces missing System.import.
 * es6-module-loader doesn't currently work with babel 6
 */
export default {
    'Contact': Contact
}